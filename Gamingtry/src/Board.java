import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class Board extends JPanel implements ActionListener {//creare panelului si implementarea eventurilor

	/**
	 * 
	 */
	private static final long serialVersionUID = 5566872736916827908L;
	private final int B_WIDTH = 600; // dimension of frame
	private final int B_HEIGHT = 600;// dimension of frame
	private final int DOT_SIZE = 10; //marimea punctului
	private final int ALL_DOTS = 900; // cate puncte
	private final int RAND_POS = 29; // pozitia
	private final int DELAY = 140; //delay de miscare

	private final int x[] = new int[ALL_DOTS];//creare array x
	private final int y[] = new int[ALL_DOTS];//creare array y
	private int dots; // puncte
	private int apple_x;//coordonata mar x
	private int apple_y;//coordonata mar y

	private boolean leftDirection = false;//spre stanga
	private boolean rightDirection = true;//spre dreapta
	private boolean upDirection = false;//in sus
	private boolean downDirection = false;//in jos
	private boolean inGame = true;//daca se afla in joc

	private Timer timer;//timp
	private Image ball;//imagine corp
	private Image apple;//imagine mar de mancat
	private Image head;//imagine cap
	public int isOver;//s-a terminat jocul

	public Board() {//constructor implicit
		addKeyListener(new TAdapter());//adaugare listener de sageti
		setBackground(Color.black);//setare background
		setFocusable(true);//setare focusabil
		setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));//setarea dimensiunii
		loadImages();//setarea imganilor
		initGame();//initializare joc
	}

	private void loadImages() { // loads images
		ImageIcon iid = new ImageIcon("dot.png");//obtine imaginea
		ball = iid.getImage();
		ImageIcon iia = new ImageIcon("apple.png");//obtine imaginea
		apple = iia.getImage();

		ImageIcon iih = new ImageIcon("head.png");//obtine imaginea
		head = iih.getImage();
	}

	private void initGame() {

		dots = 2; // how many dots

		for (int z = 0; z < dots; z++) {
			x[z] = 50 - z * 10;// location of snake
			y[z] = 50; // location of snake
		}

		locateApple();//gasirea marului

		timer = new Timer(DELAY, this);//initializare timer
		timer.start();//porninea timerului
	}

	private void locateApple() {//

		int r = (int) (Math.random() * RAND_POS);//pozitia noua random x
		apple_x = ((r * DOT_SIZE));

		r = (int) (Math.random() * RAND_POS);//pozitia noua random y
		apple_y = ((r * DOT_SIZE));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);//preia elementele anterioare

		doDrawing(g);//se apeleaza pentru a desena
	}

	private void doDrawing(Graphics g) {

		if (inGame) {//verifica daca nu e sfarsit de joc

			g.drawImage(apple, apple_x, apple_y, this);//deseneaza marul
			
			for (int z = 0; z < dots; z++) {
				if (z == 0) {
					g.drawImage(head, x[z], y[z], this);//desenare cap 
				} else {
					g.drawImage(ball, x[z], y[z], this);//desenare coada
				}
			}
			//deseneaza sarpele
			Toolkit.getDefaultToolkit().sync();//are grija ca totul e sincronizat la timp
		} else {
			gameOver(g);//se termina jocul
		}
	}

	private void gameOver(Graphics g) {

		String msg = "Game Over";//mesaj ce se afiseaza
		Font small = new Font("Helvetica", Font.BOLD, 14);//font nou
		FontMetrics metr = getFontMetrics(small);//ajusteaza fontul

		g.setColor(Color.white);//seteaza culoarea
		g.setFont(small);//seteaza fonul
		g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);//deseneaza mesajul pe suprafata
		new ButtonDemo();//instania clasa ButtonDemo

		JFrame frame = new JFrame();//creare fereastra noua
		frame.setBounds(100, 100, 437, 294);//seteaza dimensiunile ferestrei
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//metoda de inchidere
		frame.getContentPane().setLayout(null);//setarea suprafetei de lucru
		frame.setVisible(true);//setarea vizibilitatii
		
		JLabel lblPhone = new JLabel("Nickname:");//label nou
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPhone.setBounds(34, 65, 84, 23);//locatie
		frame.getContentPane().add(lblPhone);//adaugare label in fereastra

		JTextField textField_1 = new JTextField();//label nou fictiv
		textField_1.setBounds(128, 67, 193, 23);//locatie
		frame.getContentPane().add(textField_1);//adaugare in pagina
		textField_1.setColumns(10);//setare coloane fictive

		JButton btnClear = new JButton("Clear");//buton nou clear
		btnClear.setBounds(230, 136, 89, 23);//locatie
		frame.getContentPane().add(btnClear);//adaugare buton in pagina

		JButton btnSubmit = new JButton("submit");//buton nou submit
		btnSubmit.setBackground(Color.BLUE);//culoare fundal
		btnSubmit.setForeground(Color.MAGENTA);//culoare in parti 
		btnSubmit.setBounds(131, 136, 89, 23);//locatia
		frame.getContentPane().add(btnSubmit);//adaugare in pagina buton

		JLabel succes = new JLabel("");//label nou
		succes.setFont(new Font("Tahoma", Font.PLAIN, 16));//font nou
		succes.setForeground(Color.GREEN);//culoare text
		succes.setBounds(52, 192, 332, 23);//locatie
		frame.getContentPane().add(succes);//adaugare in pagina

		btnSubmit.addActionListener(new ActionListener() {//adaugare listener

			public void actionPerformed(ActionEvent arg0) {//impelemtare actiune

				if ((textField_1.getText().isEmpty()))//verifica daca e gol
					JOptionPane.showMessageDialog(null, "Data Missing");//apare caseta de dialog
				else {
					JOptionPane.showMessageDialog(null, "Data Submitted");//apare caseta de dialog
					try {
						PrintWriter pr = new PrintWriter(new FileWriter("UsersData.txt", true));//initializarea scrierii in fisier
						Calendar now = Calendar.getInstance();//instaltiere caldendar
						pr.println(textField_1.getText() + " - " + (dots - 2) + "points - " + now.getTime());//scriere in fisier
						pr.flush();//trimitere date in fisier
						pr.close();//inchidere flux
						succes.setText("Succes registered with score " + (dots - 2));//mesaj de confirmare

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});
		btnClear.addActionListener(new ActionListener() {//adaugare listener
			public void actionPerformed(ActionEvent arg0) {//implementare actiune
				textField_1.setText(null);//setare text null
			}
		});
		frame.setLocationRelativeTo(null);//pozitia initiala e centru
	}

	private void checkApple() {

		if ((x[0] == apple_x) && (y[0] == apple_y)) {//verifica daca marul a fost mancat

			dots++;//adauga la coada
			locateApple();//creaza un nou mar 
		}
	}

	private void move() {

		for (int z = dots; z > 0; z--) {
			x[z] = x[(z - 1)];//scadere din elementele sarpelui coordonata x
			y[z] = y[(z - 1)];//scadere din elementele sarpelui coordonata y
		}

		if (leftDirection) {
			x[0] -= DOT_SIZE;//pentru miscarea la stanga se va scade din x
		}

		if (rightDirection) {
			x[0] += DOT_SIZE;//pentru miscarea la dreapta se va adauga la x
		}

		if (upDirection) {
			y[0] -= DOT_SIZE;//pentru miscarea in sus se va scade din y
		}

		if (downDirection) {
			y[0] += DOT_SIZE;//pentru miscarea in jos se va adauga la y
		}
	}

	private void checkCollision() {

		for (int z = dots; z > 0; z--) {//miscare sarpe

			if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {//verificcare daca se loveste in coada
				inGame = false;
			}
		}

		if (y[0] >= B_HEIGHT) {//daca trece de partea de sus
			inGame = false;
		}

		if (y[0] < 0) {//daca trece de partea din dreapta
			inGame = false;
		}

		if (x[0] >= B_WIDTH) {//daca trece de partea de jos
			inGame = false;
		}

		if (x[0] < 0) {//daca trece de partea din stanga
			inGame = false;
		}

		if (!inGame) {//verificare oprire timer
			timer.stop();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (inGame) {//verifica daca e in joc ; implicit e

			checkApple();//verifica unde e marul si in relocalizeaza
			checkCollision();//verifica coliziunile sarpelui
			move();//misca sarpele
		}
		repaint();//actualizeaza fereastra
	}

	private class TAdapter extends KeyAdapter {//clasa ce extinde KeyAdapter pentru apasari de tasatura

		@Override
		public void keyPressed(KeyEvent e) {//suprascrie metoda de apasare ce primeste un event de apasare

			int key = e.getKeyCode();//se preia codul eventului

			if ((key == KeyEvent.VK_LEFT) && (!rightDirection)) {//se verifica daca e sageata la stanga apasata
				leftDirection = true;
				upDirection = false;
				downDirection = false;
			}

			if ((key == KeyEvent.VK_RIGHT) && (!leftDirection)) {//se verifica daca e sageata la dreapta apasata
				rightDirection = true;
				upDirection = false;
				downDirection = false;
			}

			if ((key == KeyEvent.VK_UP) && (!downDirection)) {//se verifica daca e sageata in jos apasata
				upDirection = true;
				rightDirection = false;
				leftDirection = false;
			}

			if ((key == KeyEvent.VK_DOWN) && (!upDirection)) {//se verifica daca e sageata in sus apasata
				downDirection = true;
				rightDirection = false;
				leftDirection = false;
			}
		}
	}
}