import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonDemo extends JPanel implements ActionListener {//declararea clasei 
	/**
	 * 
	 */
	private static final long serialVersionUID = 8134276530836826216L;
	protected JButton b1, b2;//declararea butonului vizibil doar in clasa
	JFrame frame; //declararea ferestrei
	public ButtonDemo() {//constructorul clasei
		frame = new JFrame("Alege");//instantiere frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//metoda de inchidere a ferestrei
		frame.pack();//lipirea impreuna a tutror componentelor si ajustarea ferestrei
		frame.setResizable(false);//nu poate fi modificata dimensiunea ferestrei

		b1 = new JButton("", new ImageIcon("new.png"));//instantierea si adaugarea imaginii de tip Icon
		b2 = new JButton("", new ImageIcon("block.png"));//instantierea si adaugarea imaginii de tip Icon

		b1.addActionListener(this);//adaugarea listenerului primului buton (inima)
		b2.addActionListener(this);//adaugarea listenerului a doilea buton (cruce)

		b1.setToolTipText("Click pentru a porni din nou.");//tool tip creare
		b2.setToolTipText("Click pentru a �nchide jocul");//tool tip creare
		add(b1);//adaugarea butonului 1 in panel
		add(b2);//adaugarea butonului 2 in panel
		setOpaque(true); // content panes must be opaque
		frame.setContentPane(this);//stabilirea locatiei contentului ferestrei
		frame.setSize(400,180);//dimensiunea ferestrei
		frame.setLocationRelativeTo(null);//la centru pozitionare
		frame.setVisible(true);//se face vizibil
	}

	public void actionPerformed(ActionEvent e) {//implementarea metodei de actiuni generales

		if (e.getSource()== b1) {//verificarea daca e event de la butonul b1
			frame.setVisible(false);//se face invizibila fereastra
			frame.dispose();//se distruge
			new Snake();//se instantiata clasa Snake pentru un joc nou
		}
		if (e.getSource()== b2) {//verificarea daca e event de la butonul b2
			System.exit(0);//iese din aplicatie
		}
	}
	public static void main(String[] args) {//metoda main 
		new ButtonDemo();//se instantiaza clasa ButtonDemo
	}
}